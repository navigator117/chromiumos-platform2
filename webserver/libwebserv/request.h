// Copyright 2014 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef WEBSERVER_LIBWEBSERV_REQUEST_H_
#define WEBSERVER_LIBWEBSERV_REQUEST_H_

#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include <base/callback_forward.h>
#include <base/macros.h>
#include <base/memory/ref_counted.h>
#include <base/memory/scoped_ptr.h>
#include <chromeos/errors/error.h>
#include <libwebserv/export.h>

struct MHD_Connection;

namespace libwebserv {

class ProtocolHandler;

using PairOfStrings = std::pair<std::string, std::string>;

// This class represents the file information about a file uploaded via
// POST request using multipart/form-data request.
class LIBWEBSERV_EXPORT FileInfo final {
 public:
  const std::string& GetFileName() const { return file_name_; }
  const std::string& GetContentType() const { return content_type_; }
  const std::string& GetTransferEncoding() const { return transfer_encoding_; }
  void GetData(
      const base::Callback<void(const std::vector<uint8_t>&)>& success_callback,
      const base::Callback<void(chromeos::Error*)>& error_callback);

 private:
  friend class Server;

  LIBWEBSERV_PRIVATE FileInfo(ProtocolHandler* handler,
                              int file_id,
                              const std::string& request_id,
                              const std::string& file_name,
                              const std::string& content_type,
                              const std::string& transfer_encoding);

  ProtocolHandler* handler_{nullptr};
  int file_id_{0};
  std::string request_id_;
  std::string file_name_;
  std::string content_type_;
  std::string transfer_encoding_;
  std::vector<uint8_t> data_;

  DISALLOW_COPY_AND_ASSIGN(FileInfo);
};

// A class that represents the HTTP request data.
class LIBWEBSERV_EXPORT Request final {
 public:
  ~Request();

  // Gets the request body data stream. Note that the stream is available
  // only for requests that provided data and if this data is not already
  // pre-parsed by the server (e.g. "application/x-www-form-urlencoded" and
  // "multipart/form-data"). If there is no request body, or the data has been
  // pre-parsed by the server, the returned stream will be empty.
  const std::vector<uint8_t>& GetData() const;

  // Returns the request path (e.g. "/path/document").
  const std::string& GetPath() const { return url_; }

  // Returns the request method (e.g. "GET", "POST", etc).
  const std::string& GetMethod() const { return method_; }

  // Returns a list of key-value pairs that include values provided on the URL
  // (e.g. "http://server.com/?foo=bar") and the non-file form fields in the
  // POST data.
  std::vector<PairOfStrings> GetFormData() const;

  // Returns a list of key-value pairs for query parameters provided on the URL
  // (e.g. "http://server.com/?foo=bar").
  std::vector<PairOfStrings> GetFormDataGet() const;

  // Returns a list of key-value pairs for the non-file form fields in the
  // POST data.
  std::vector<PairOfStrings> GetFormDataPost() const;

  // Returns a list of file information records for all the file uploads in
  // the POST request.
  std::vector<std::pair<std::string, const FileInfo*>> GetFiles() const;

  // Gets the values of form field with given |name|. This includes both
  // values provided on the URL and as part of form data in POST request.
  std::vector<std::string> GetFormField(const std::string& name) const;

  // Gets the values of form field with given |name| for form data in POST
  // request.
  std::vector<std::string> GetFormFieldPost(const std::string& name) const;

  // Gets the values of URL query parameters with given |name|.
  std::vector<std::string> GetFormFieldGet(const std::string& name) const;

  // Gets the file upload parameters for a file form field of given |name|.
  std::vector<const FileInfo*> GetFileInfo(const std::string& name) const;

  // Returns a list of key-value pairs for all the request headers.
  std::vector<PairOfStrings> GetHeaders() const;

  // Returns the value(s) of a request header of given |name|.
  std::vector<std::string> GetHeader(const std::string& name) const;

  // Returns the value of a request header of given |name|. If there are more
  // than one header with this name, the value of the first header is returned.
  // An empty string is returned if the header does not exist in the request.
  std::string GetFirstHeader(const std::string& name) const;

 private:
  friend class Server;

  LIBWEBSERV_PRIVATE Request(ProtocolHandler* handler,
                             const std::string& url,
                             const std::string& method);

  ProtocolHandler* handler_{nullptr};
  std::string url_;
  std::string method_;
  std::vector<uint8_t> raw_data_;
  bool last_posted_data_was_file_{false};

  std::multimap<std::string, std::string> post_data_;
  std::multimap<std::string, std::string> get_data_;
  std::multimap<std::string, std::unique_ptr<FileInfo>> file_info_;
  std::multimap<std::string, std::string> headers_;

  DISALLOW_COPY_AND_ASSIGN(Request);
};

}  // namespace libwebserv

#endif  // WEBSERVER_LIBWEBSERV_REQUEST_H_

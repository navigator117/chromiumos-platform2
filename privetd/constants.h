// Copyright 2014 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef PRIVETD_CONSTANTS_H_
#define PRIVETD_CONSTANTS_H_

namespace privetd {

namespace errors {

extern const char kPrivetdErrorDomain[];
extern const char kInvalidClientCommitment[];

}  // namespace errors

}  // namespace privetd

#endif  // PRIVETD_CONSTANTS_H_

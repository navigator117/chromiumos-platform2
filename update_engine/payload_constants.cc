// Copyright 2014 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "update_engine/payload_constants.h"

namespace chromeos_update_engine {

const char kDeltaMagic[] = "CrAU";
const char kBspatchPath[] = "bspatch";

};  // namespace chromeos_update_engine

// Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "shill/mock_ip_address_store.h"

namespace shill {

MockIPAddressStore::MockIPAddressStore() {}
MockIPAddressStore::~MockIPAddressStore() {}

}  // namespace shill

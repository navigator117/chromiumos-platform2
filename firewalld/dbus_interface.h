// Copyright 2014 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FIREWALLD_DBUS_INTERFACE_H_
#define FIREWALLD_DBUS_INTERFACE_H_

namespace firewalld {

const char kFirewallInterface[] = "org.chromium.Firewalld";
const char kFirewallServicePath[] = "/org/chromium/Firewalld";
const char kFirewallServiceName[] = "org.chromium.Firewalld";

}  // namespace firewalld

#endif  // FIREWALLD_DBUS_INTERFACE_H_

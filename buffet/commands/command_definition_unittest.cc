// Copyright 2014 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "buffet/commands/command_definition.h"

#include <gtest/gtest.h>

TEST(CommandDefinition, Test) {
  auto params = std::make_shared<buffet::ObjectSchema>();
  auto results = std::make_shared<buffet::ObjectSchema>();
  buffet::CommandDefinition def("powerd", params, results);
  EXPECT_EQ("powerd", def.GetCategory());
  EXPECT_EQ(params, def.GetParameters());
  EXPECT_EQ(results, def.GetResults());
}

// Copyright 2014 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "trunks/mock_authorization_session.h"

namespace trunks {

MockAuthorizationSession::MockAuthorizationSession() {}
MockAuthorizationSession::~MockAuthorizationSession() {}

}  // namespace trunks
